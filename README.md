# README #

Development Information for IMS Messsysteme GmbH

Just some guides to point developers in the right direction.

## Supported OS ##
Windows 7 and greater
Windows Server 2008 and greater
Linux Ubuntu 14.04 and greater


## Configuration Files ##
All persistent configuration files for applications should be located in:
```%ALLUSERSPROFILE%/IMS Messsysteme/<product>/```
or for Linux:
```/etc/IMS Messsysteme/<product>/```

Where <product> is the product name.

## Set Developer Machine ##
to switch console output. Add an envioment setting 
add ```IMS_DEVELOPER_MACHINE = 1``` to the envioment settings. 

How to do it checkout here http://www.computerhope.com/issues/ch000549.htm


## Directory Layout for small projects ##
```
doc
client
   src
   test
server
   src
   test
```

## Possible new folder structure for big projects ##

```
*** => .gitignore

.idea
.vscode
.eslintignore
.eslintrc.json
.gitignore
.editorconfig
package.json
gulpfile.js

doc  
  flowDesignImage.jpg
  allLicense.csv

dist
  *.7z

node_modules
   .bin
      node.exe
      npm.cmd

src
  manual
    htmlHelpProject
  client
    manual ***
      htmlHelpResult 
    lib
      js
      css
    js
      controller
      service
      filter
      directive
      app.js
    css
      *.css|(*.less|*.sass)
    template
      *.html
    image
      favicon.ico
      imsLogo.svg 
    font
    index.html
  server
    rest
    configuration
    server.js

test
  client
  server
  e2e
    rest
    dashboard
    comparision

bin
  7zip.exe
  7zip.dll

task
  *.cmd
```

## Tools ##
* NodeJS https://nodejs.org/dist/v4.4.2/node-v4.4.2-x86.msi => currently used javascript version is: ECMAScript 5.1 (ES5), NodeJS 4.4.x and nearly all modern browsers support this version, feature compatibility table: http://kangax.github.io/compat-table moving to NodeJS 5.x is on the way... (moving to ES6 is possible, when Chrome, Firefox, Safari, and Edge support nearly all features)
* install compiler  (Visual Studio 2015 with C++ \\sbnas01\share\install\MSDN\VisualStudio) or VisualCppBuildTools2015
* git: https://git-scm.com/downloads
* python: https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi or any 2.7.x
* IMORTANT: Visual Studio 2010 Redistributables for C++ (for ZMQ) => \\sbnas01\share\install\MSDN\VisualStudio\VS2010\vcredist_x86.exe
*** The target system may need all CPP redistributables or else the programs will silently fail to start !**




gulpfile.js should contain global thresholds for istanbul: 80
so that a coverage rate of 80% is reached for all lines, functions, branches and statements.

## JavaScript Style Guide ###
https://github.com/airbnb/javascript/tree/master/es5

For basic code style we use http://editorconfig.org/. The current .editorconfig file is also in this repro.

## Problems with corporate proxy

create files in your user directory (example: C:\Users\wb):
```
.npmrc:
proxy=http://192.168.10.9:3128/
https-proxy=http://192.168.10.9:3128/
##registry=https://registry.npmjs.org
##registry=http://m-w7-opti-th:4873/

.gitconfig:
[http]
	proxy = http://192.168.10.9:3128
[url "https://"]
	insteadOf = git://
[https]
	proxy = http://192.168.10.9:3128 

.bowerrc:
{
    "directory": "app/bower_components",
    "registry": "http://bower.herokuapp.com",
    "proxy":"http://192.168.10.9:3128/",
    "https-proxy":"http://192.168.10.9:3128/",
    "strict-ssl": false
}
```
## Git Workflow ##
GIT-Guide: http://rogerdudler.github.io/git-guide/

Checkout project

* open git bash
* change directory (cd Project/Bitbucket/)
* checkout project (git clone https://imswb@bitbucket.org/imsgmbh/fileporter.git FilePorter)
* change directory to the project (cd FilePorter)
* install the used packages (npm install)



For each feature:

Get newest files

* git pull
* gulp test

Create branch for feature "EnoughStorageInDestinationFolder" and switch to it at the same time

* git checkout -b EnoughStorageInDestinationFolder

Develop new feature

* gulp test

* git status
* git add readme.md
* git commit -m "dies und das"

Push feature to remote repository

* git push -u origin EnoughStorageInDestinationFolder

Create Pullrequest from branch in Bitbucket

Checkout the master and merge with the newest features

* git checkout master

* git pull

## Create new repositiory and first commit ##
* create a BitBucket repository, owner is: imsgmbh
* mkdir /path/to/your/project
* cd /path/to/your/project
* git init
* git remote add origin https://yourname@bitbucket.org/imsgmbh/reponame.git


## Merge Conflicts ##
* checkout your branch: git checkout mybranch
* merge with master: git merge master
* edit conflicted files to contain the right code
* resolve conflict by adding all edited files: git add .
* commit the files: git commit -m "conflict resolved"
* git push -u origin mybranch

## Tests ##
Coverage is measured with: testCoverage.cmd
Coverage is validated with: validateCoverage.cmd

Some environment settings are used to define behaviour in tests:

* TESTSTAGE=ALPHA => unstable version 
* TESTSTAGE=BETA => incomplete version
* TESTSTAGE=RC => ready for release

Alternative: settings per command line.

Command line options have highest priority.
Permanent changes to standard options should be applied to ServiceManager.
Temporary changes can be applied to package.json and will be overwritten by software update.

Service-Client-Mocks can be loaded at runtime (command line option) instead of real implementation:
-MOCKML mlClientMock.js
-MOCKSP spClientMock.js

## Issue Workflow ##
1. Move issue in Jira to *In Progress*
2. Create branch for issue with name *[IssueIdInJira]* (example: MMSG-238)
3. Work on issue and commit/push to branch
4. Move issue in Jira to *Review* and create Pull Request in Bitbucket with titel *[IssueIdInJira] - [IssueDescInJira]*
5. Wait for other developer to review pull request and fix/change until approved
6. Merge pull request and move issue to *Done*